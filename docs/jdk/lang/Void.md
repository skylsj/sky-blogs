> Void 类的代码如下：

```java
package java.lang;

/**
 * The {@code Void} class is an uninstantiable placeholder class to hold a
 * reference to the {@code Class} object representing the Java keyword
 * void.
 * ( Void类是一个不可实例占位符类,用于保存对表示Java关键字void的{@code class}对象的引用。)
 * @author unascribed
 * @since JDK1.1
 */
public final class Void {

    /**
     * The {@code Class} object representing the pseudo-type corresponding to
     * the keyword {@code void}.
     * ( 所述Class表示对应于所述关键字的伪类型的对象void 。)
     */
    @SuppressWarnings("unchecked")
    public static final Class<Void> TYPE = (Class<Void>) Class.getPrimitiveClass("void");

    /*
     * The Void class cannot be instantiated.
     * ( Void类不能被实例化。)
     */
    private Void() {
    }
}
   /*
    * 解析：{@code Void}类是final的,不可继承，并且构造是私有的，也不能 new。
    * Void是void关键字的包装类。
    *
    * 作用：
    * 使用场景一：调用泛型接口不需要返回值时可以用。
    * <blockquote><pre>
    * new java.security.PrivilegedAction<Void>() {
    *     public Void run() {
    *        c.setAccessible(true);
    *            return null;
    *      }
    * }
    * </pre></blockquote><p>
    * 使用场景二：通过反射获取返回值为void的方法。
    * <blockquote><pre>
    *     if (method.getReturnType().equals(Void.TYPE))
    * 
    *     if (void.class.equals(method.getReturnType()))
    * </pre></blockquote><p>
    *
    * method.getReturnType()==void.class     √
    * method.getReturnType()==Void.Type      √
    * method.getReturnType()==Void.class     X
    *
    * 使用场景三：用于无值的Map,Map<T,Void>这样的map和Set<T>具有一样的功能。
    *
    */
```