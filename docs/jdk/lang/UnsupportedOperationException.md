> UnsupportedOperationException 类的代码如下：

![Image](/UOE.jpg)


```java
package java.lang;

/**
 * Thrown to indicate that the requested operation is not supported.
 */
public class UnsupportedOperationException extends RuntimeException {
    /**
     * Constructs an UnsupportedOperationException with no detail message.
     */
    public UnsupportedOperationException() {
    }

    /**
     * Constructs an UnsupportedOperationException with the specified
     * detail message.
     *
     * @param message the detail message
     */
    public UnsupportedOperationException(String message) {
        super(message);
    }

    public UnsupportedOperationException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnsupportedOperationException(Throwable cause) {
        super(cause);
    }

    static final long serialVersionUID = -1242599979055084673L;
}
    /*
     * 解析：不支持的操作异常，和其它RuntimeException异常一样，unchecked 异常。
     *      JDK内部主要运用到集合框架（Collection）里。如：
     *
     *  String[] array = {"1","2","3","4","5"};
     *  List<String> list = Arrays.asList(array);
     *  list.add("6");
     *
     * 作用：
     *
     * 使用场景一：在私有构造函数内抛此异常，以防止外部通过反射强行实例化。
     *
     *  private SingletionClass(){
     *     throw  UnsupportedOperationException("非法的操作异常，此类不能被实例化。");
     *  }
     *
     * 使用场景二：通过分支进入不同业务场景时，如果没有默认的场景，在出现未知场景时，可以用此异常代替。
     *
     * Exemple:
     * 
     *  if("male".equals(gender)){
     *     return "男";
     *   }
     * if("female".equals(gender)){
     *     return "女";
     *  }
     *  throw  UnsupportedOperationException("非法的操作异常，性别有误。");
     * 
     * 【注】 switch语句 ,枚举等都可以用到。
     *
     *
     */

```